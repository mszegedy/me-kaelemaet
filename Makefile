grammar.pdf: TEXFILES grammar.bbl
	@xelatex grammar.tex

TEXFILES: *.tex chapters/*.tex lexicon/roots.tex
LEXFILES: lexicon/*.txt

lexicon/roots.tex: LEXFILES
	cd lexicon; \
  cp roots.txt roots.txt.bak; \
	./transpile.py

grammar.bbl: bibliography.bib myhumannat.bst
	@xelatex grammar.tex
	bibtex grammar
	@xelatex grammar.tex
