\chapter{Phonology and orthography}
\section{Inventory and allophony}
\subsection{Consonants}
\begin{fullwidth}\ipa
  \tabcolsep=1.8em
  \begin{tabular}{
      @{\hskip 1em}c
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      @{\hskip 1em}}
    \toprule\midrule
    Labial
    & \multicolumn{2}{c}{\makebox[0pt]{Alveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{Postalveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{Prepalatal}}
    & \multicolumn{2}{c}{\makebox[0pt]{Velar}} \\\midrule
    m & n &&&& ȵ & \ortho{\'n} & ŋ & \\
    p & t && t͡ʃ & \ortho{\v{c}} & t͡ɕ & \ortho{\'c}& k & \\
    & s && ʃ & \ortho{\v{s}} & (ɕ) & \ortho{\'s} \\
    w & ð̞ l r & \ortho{d l r} &&& ð̞ʲ j & \ortho{g j} & ɣ\,\~{}\,ɰ & \ortho{v}
    \\\bottomrule
  \end{tabular}
\end{fullwidth}
\vspace{1em}

\noindent Me Kälemät's\histnote{\mk{\'c} is PU\,\recon{ś}, based on widespread
  observations that it's distributed like a stop. The allophony and marginal
  phonemicity of \mk{\'s} is artistic license. \mk{d~g} are
  PU\,\recon{d},\,\recon{ď}, based on Pystynen's observations that they're
  distributed like sonorants, and have mostly sonorant reflexes. \mk{v} is
  PU\,\recon{x}, and the realization is artistic license, along with the
  observation that it's distributed like a sonorant.} consonants don't change
much based on where they are. The stops and affricates \mbox{/\upa{p t t͡ʃ t͡ɕ
    k}/} can occur as geminates anywhere in the middle of a word, but not at the
edges. /\upa{t͡ɕ}/ becomes [\upa{ɕ}] at the end of syllables, where it's spelled
\ortho{\'s}. Between vowels, it wavers between either, as [\upa{t͡ɕ\,\~{}\,ɕ}],
and is still spelled~\ortho{\'c}. It is also marginally phonemic in a small set
of Indo-Iranian loanwords that begin with [\upa{ɕ}], such as
\mk{\'s\"a\'c\'cem\"a} and \mk{\'s\"eta}. /\upa{ɣ}/ is realized as [\upa{ɰ}]
after vowels, and [\upa{ɣ}] after consonants. /\upa{l}/ is realized as [\upa{l}]
in most environments, but as [\upa{ɫ}] when \upa{/\,ȣ\_(C)ɤ}.

\subsection{Vowels}\label{s:vowels}
Me Kälemät has a system of eight vowels in the first syllable of a stem, and
only two vowels elsewhere. The non-initial open vowel /\upa{a}/ is realized as
[\upa{æ}]~\ortho{\"a} in stems that begin with one of \mbox{/\upa{i y e æ}/},
and [\upa{ɒ}]~\ortho{a} otherwise (i.e.~in stems that begin with \mbox{/\upa{u ɤ
    o ɒ}/}). Non-initial /\upa{ɤ o}/ can be reduced all the way to [\upa{ə ɵ}]
in rapid or casual speech.
\marginnote{\ipa\normalsize\centering
  \begin{tabular}{llrr}
    \toprule
    i y & \ortho{i \"u} & \multicolumn{1}{r}{u} & \ortho{u} \\
    e & \ortho{e} & ɤ o & \ortho{\"e o} \\
    æ & \ortho{\"a} & ɒ & \ortho{a} \\\bottomrule
  \end{tabular}\\[0.5em]Initial vowels\\\vspace{1em}
  \begin{tabular}{cc}\toprule
    ɤ o & \ortho{e o} \\
    a   & \ortho{a/\"a} \\\bottomrule
  \end{tabular}\\[0.5em]Non-initial vowels\vspace{1em}}%
\histnote{%
  This is the analysis of the PU vowel system presented in \citet{A} and
  \citet{Ued}, but with Pystynen's well-argued \recon{ë/e̮} rather than the
  traditional \recon{ï/i̮}. The analysis of \recon{a} as [\upa{ɒ}] is one that
  we've absorbed partly through \citet{Piv}, which argues it based on the
  majority of its reflexes being rounded, and partly just through Hungarian
  chauvinism. The weak second-syllable vowel, reflected as PSá~\recon{ë}, is
  reconstructed as \recon{ë} for aesthetic reasons (though the Sámic reflex does
  give it some motivation). In our actual reconstructions, we stick to writing
  it as \recon{ə}, for the sake of familiarity.
}%

There are certain stems that begin with /\upa{i}/ as the first vowel, but have
[\upa{ɒ}] for their second vowel, and thus for all subsequent instances of
/\upa{a}/. Thus, there are really \emph{two} /\upa{i}/ phonemes:
/\upa{i\subscr{1}}/, after which /\upa{a}/ is realized as [\upa{æ}], and
/\upa{i\subscr{2}}/, after which it is realized as [\upa{ɒ}]. The Latin and
Cyrillic orthographies distinguish these indirectly by notating the difference
between non-initial [\upa{æ}] and [\upa{ɒ}], while the Inuktitut syllabics
orthography does not distinguish the two. /\upa{i\subscr{2}}/ is rare in native
roots, but overwhelmingly common in loanwords from languages that do not have
[\upa{æ}], such as \mk{ipsevolovja}, from Latin \textit{psychologia}.

\section{Phonotactics}
Me Kälemät has three kinds of syllables, each with different rules:
stem-initial syllables, non-initial stem syllables, and suffix syllables.
Stem-initial\histnote{These phonotactics are from \citet{A}, alongside the
  observation that loanwords such as PU~\recon{rep\"a(\'sɜ)} can begin with
  \recon{r}.} syllables are of the form (C)V(C). The initial consonant, if
present, may be any of Me Kälemät's consonants, apart from \mk{d}, \mk{v},
\mk{ŋ}, and \mk{r}, though they can all become initial consonants in loanwords.
The vowel can be any of the eight different stem-initial vowels discussed
above~(\S\ref{s:vowels}). The final consonant can be anything, so long as it
forms a legal consonant cluster with the next consonant.

Non-initial
\histnote{An explanation of this suffixing phonology can be found in
  \citet{A}.}%
\marginnote{\vspace{1em}}%
stem syllables are of the form CV(C), or just CV(w) if they are stem-final
(which they usually are). In either case, the beginning consonant may be
anything, and in the non-stem-final case, so may the ending consonant. If the
stem has consonantal suffixes attached, then may only be a single consonant
that's part of the set of consonants that can occur in suffixes in general (see
below), or \mk{-j}\{\mk{m,\,n}\}, when the \mk{-j-} and the nasal are two
separate suffixes.

The
\marginnote{\centering\normalsize\ipa
  \begin{tabular}{llll}
    \toprule
    m & n   &    & ŋ \\
    p & t   & t͡ɕ & k \\
      & s   &    &   \\
    w & l r & j  &   \\\bottomrule
  \end{tabular}\\[0.5em]Suffix consonants\\[1em]
  \begin{tabular}{c}\toprule
    ɤ o \\
    a \\\bottomrule
  \end{tabular}\\[0.5em]Suffix vowels\vspace{1em}}%
set of consonants that suffixes can have is far smaller than the ones that can
occur in stems. Suffixes can be just a single consonant, or a -CV syllable, or
even certain -CCV syllables. They are shown on the right, along with the two
vowels that suffixes can have. There can be multisyllabic suffixes, but they're
pretty much always made up of multiple shorter suffixes. As noted earlier, long
\mbox{/\upa{p t t͡ʃ t͡ɕ k}/} can't be single-consonant suffixes on their own.
Given the rules below, the amount of suffixes allowed is 14~-C suffixes, 36~-CV
suffixes, and 236~-CCV suffixes, for a total of 286 single-syllable and
single-consonant suffixes.

Stems and suffixes follow the same rules for what consonants can be next to
each other. These rules are:
\begin{enumerate}
  \item Two of the same consonant aren't allowed after each other (unless you're
    talking about long /\upa{p t t͡ʃ t͡ɕ k}/).
  \item When the cluster is all part of one morpheme, obstruents can't go before
    sonorants. When there's a morpheme boundary, however, /\upa{s ʃ}/ are
    allowed to precede sonorants.
  \item Obstruents can't go before long /\upa{p t t͡ʃ t͡ɕ k}/ either.
    \histnote{Few of these rules are fully supported by comparative evidence. None
      of them contradict any evidence either, however. Rules~1, 5, and 7--10 are
      fully supported in \citet{A}. Rule~2 is almost supported fully, but the
      inclusion of \recon{\v{s}} is artistic license. Rule~3 seems obvious and
      fully supported, but we haven't seen it noted anywhere. Rule~4 is fully
      supported apart from the inclusion of \recon{ŋx}, which is based on
      \citet{Png}'s suggestion that the cluster may be responsible for Ugric
      \recon{ŋk} and Permic \recon{∅} from PU~\recon{ŋ}. Rule~6 was puzzled out
      through observation, though a more well-researched formal statement of it
      must exist somewhere.}
  \item Sonorants can't go after nasals, except in the cluster /\upa{ŋɣ}/.
  \item The only things that can go before /\upa{p}/ are /\upa{m}/, and all the
    non-nasal sonorants besides /\upa{w}/.
  \item /\upa{n t t͡ʃ}/ aren't allowed before /\upa{t͡ɕ}/, and /\upa{ȵ t͡ɕ}/ aren't
    allowed before /\upa{t͡ʃ}/.
  \item The only nasal allowed before /\upa{k}/ is /\upa{ŋ}/.
  \item /\upa{t t͡ʃ t͡ɕ}/ aren't allowed before /\upa{s ʃ}/.
  \item /\upa{wp}/ and /\upa{wm}/ are not allowed.
  \item While normally three consonants are forbidden from being next to each
    other, -\mk{j}CC- is allowed when there's a morpheme boundary somewhere in
    the cluster.
\end{enumerate}

\section{Prosody}
Similar\histnote{This section, as well as \S\ref{s:compounds}, takes the
  generally accepted position that PU had initial stress, and expands it
  entirely based on artistic license, and on simulating modern Uralic
  languages.} to the prosody of many Uralic languages, initial syllables of
polysyllabic words are stressed using intonation, and also have their vowels
extended slightly. Otherwise, all syllables take up roughly the same time. The
pitch across a single word is always descending, with a relatively dramatic drop
after the first syllable, and a less dramatic descent in subsequent syllables.
From word to word, the pitch jumps somewhat, to accentuate the first syllable of
each new word. The sentence \mk{«Pu\v{c}a rep\"a\'cen pon\v{c}e uwana.»}
meaning, ``The little fox's tail soaks in the stream,'' has the following
melody:

\begin{center}
  \begin{tikzpicture}
    \contour[contour raise=0.5cm]
            {|[10]Puča|[6] |[8]re|[5]päćen|[4] |[6]ponče|[2] |[4]u|[1]wana.|[0]}
  \end{tikzpicture} 
\end{center}\vspace{-1em}

\noindent Elements of compounds have special rules for the melodies of their
components, covered in \S\ref{s:compounds}.

\section{Morphophonology}
\subsection{Suffixes}
All of Me Kälemät's morphology is either compounding or suffixing. When a suffix
is added to a stem ending in \mk{-a/\"a}, there's no alterations; they are
simply glued together. However, stems that end in \mk{-e} lose the final vowel,
so this will often create illegal clusters, usually of three consonants. For
two-consonant clusters, the \mk{-e-} is usually just reinserted if the cluster
isn't allowed, as in \mk{nime-je} > \mk{nimeje} and \mk{w\"ake-ŋ\"a} >
\mk{w\"akeŋ\"a}.
\histnote{The two-consonant elision rules are mostly artistic license, but an
  attempt is made to be consistent with the compounds given in \citet{A}. The
  nasal deletion rule is from the \morp{1sg} possessor endings reconstructed in
  \citet{J}, in which the case suffixes \recon{-m} and \recon{-n} cause the
  \recon{-m-} in the possessor ending to elide.}
However, if they are two different nasals, then the second one gets deleted. If
they are two of the same stop, affricate, or nasal, then they collapse into one
long version of that consonant. This is the only way to get long nasals.

In three-consonant clusters, if the first consonant is \mk{j}, then
it's no problem; that's allowed. In situations where the first two consonants
don't belong to a word stem together, then the only solution for breaking up the
cluster is to reinsert the \mk{-e}. But if they do, then they can get deleted to
make room. Only nasals, stops, and affricates can get deleted this way, and only
if it would make a cluster that's allowed. First, you try deleting the first
consonant, as in \mk{man\v{c}e-ta} > \mk{ma\v{c}ta} and \mk{l\"ep\'ce-ta} >
\mk{l\"e\'sta}.
\histnote{These rules are generalized from the three examples given (from
  \citet{A}). We observed no elision outside of \recon{-CCə} stems, so that is
  disallowed.}
If you can't do that, either because it's not the kind that you
can delete or it would make an illegal cluster, you try deleting the second
consonant, as in \mk{kulke-ta} > \mk{kulta}. If neither of those work, you
reinsert the~\mk{-e-}.

\subsection{Compounds}\label{s:compounds}
Me Kälemät\histnote{This entire section is artistic license, though it is
  based in a few real concepts. The noun incorporation represents PU's
  ``nominative objects'', and the verb serialization and semantic compounding
  represents PU's tendency to form semantic compounds consisting of two
  elements. The classifying compounds represent the equivalence of adjectives
  and nouns in PU.} forms compounds for several reasons: to incorporate nouns
into verbs, to string together verbs for serial verb constructions, to string
together nouns with other classifying nouns, and to string together concepts
into compounds that represent the concept that unites them. Each element of a
compound behaves phonotactically like its own word, but the pitch melody used
for the compound is different depending on what kind of compound it is.

Noun incorporation and verb serialization use the same kind of melody, as they
are part of the same kind of construction. In these, the pitch descends as
normal, but halts its descent at the first syllable of each new element in the
compound. These syllables, like the initial syllables of independent words, are
slightly longer than other syllables. The sentence
\mk{«Poskene·peljäne·ńale·jüräjme.»} meaning, ``I licked and nipped at their
cheek and ears,'' has the melody:

\begin{center}
  \begin{tikzpicture}
    \contour[contour raise=0.5cm]
            {|[10]Poskene|[7]·pel|[7]jäne|[5]·ńa|[5]le|[3]·jü|[3]räjme.|[0]}
  \end{tikzpicture} 
\end{center}\vspace{-1em}

\noindent This freely varies with a similar melody, where the pitch rises before
the initial syllable of each element, almost up to the starting pitch of the
previous element, and otherwise descends normally.

\begin{center}
  \begin{tikzpicture}
    \contour[contour raise=0.5cm]
            {|[10]Poske|[6]ne|[8]·peljä|[4]ne|[6]·ńa|[3]le|[5]·jüräjme.|[0]}
  \end{tikzpicture} 
\end{center}\vspace{-1em}

\noindent Classifier-classified compounds have a more-or-less level, high tone
for the classifiers, and then start falling after the first syllable of the
classified elements. The phrase \mk{uwaŋa·p\"ajew·puna} `flowing, willowy fur'
has the melody:

\begin{center}
  \begin{tikzpicture}
    \contour[contour raise=0.5cm]
            {|[10]uwaŋa·päjew·pu|[9]na|[4]}
  \end{tikzpicture} 
\end{center}\vspace{-1em}

\noindent Finally, multi-concept compounds have a falling tone throughout, not
distinguishing elements from each other at all.

\section{Orthography}
\subsection{Latin alphabet}
Given that acronyms are an occasional component of the language, letter names
are worth clarifying. The names of the 27 letters of the Me Kälemät alphabet,
along with their ASCII representation if it differs, are:
\begin{center}
  \begin{tabular}{c@{\hskip 2pt}cc@{\hskip 2pt}cl}
    A & a &&& \mk{a} \\
    \"A & \"a & Ae & ae & \mk{\"a} \\
    \'C & \'c & Cy & cy & \mk{\'ce} \\
    \v{C} & \v{c} & Cz & cz & \mk{\v{c}e} \\
    D & d &&& \mk{\'cade} or \mk{de} \\
    E & e &&& \mk{e} \\
    \"E & \"e & Eo & eo & \mk{\"e} \\
    G & g &&& \mk{ge} \\
    I & i &&& \mk{i} \\
    J & j &&& \mk{je} \\
    K & k &&& \mk{ka} \\
    L & l &&& \mk{\"ale} \\
    M & m &&& \mk{\"ame} \\
    N & n &&& \mk{\"ane} \\
  \end{tabular}\hspace{1em}
  \begin{tabular}{c@{\hskip 2pt}cc@{\hskip 2pt}cl}
    \'N & \'n & Ny & ny & \mk{\"a\'ne} \\
    Ŋ & ŋ & Nz & nz & \mk{\"aŋe} \\
    O & o &&& \mk{o} \\
    P & p &&& \mk{pe} \\
    R & r &&& \mk{\"are} \\
    S & s &&& \mk{\"ase} \\
    \'S & \'s & Sy & sy & \mk{\'sim\'ce} or \mk{\'cim\'ce} \\
    \v{S} & \v{s} & Sz & sz & \mk{\"a\v{s}e} \\
    T & t &&& \mk{te} \\
    U & u &&& \mk{u} \\
    \"U & \"u & Ue & ue & \mk{\"u} \\
    V & v &&& \mk{wave} or \mk{ve} \\
    W & w &&& \mk{we} \\
  \end{tabular}
\end{center}
\noindent The somewhat unusual ASCII orthography is designed so that words have
the same alphabetical order regardless of whether they are using the standard
Latin letters or the ASCII ones.

Apart from the above-mentioned graphemes and the behavior of the phonemes they
represent, worth discussing is Me Kälemät punctuation and typesetting. While
loanwords are generally rendered phonetically, it is also acceptable to render
them in their original spelling, in which case they are italicized unless they
are a proper noun, and any appended prefixes or suffixes are connected with a
colon. Acronyms likewise receive suffixes with a colon. Elements of compounds
are separated with the mid-dot ·. Quotes are done with «~and~», and nested
quotes are done with ‹~and~›. Punctuation does not change in quotations, affixes
are added directly to them without any other indication, clitics are added to
them via hyphens, and they do not change the following capitalization; in short,
they act exactly like words. In ASCII, · is replaced with~\texttt{-}, «~and~»
are both replaced with~\texttt{"}, and ‹~and~› are both replaced
with~\texttt{'}. Hence:
\bigexample{%
  «El\"a SMS·meve·pelek me \textit{auditorium}:na.» monajme Jonathan:ŋ. \par
  \texttt{"Elae SMS-meve-pelek me auditorium:na." monajme Jonathan:nz.}}{%
  ``Don't be afraid to send me an SMS at the auditorium,'' I told Jonathan.}

\subsection{Cyrillic alphabet}
\begin{fullwidth}
  \tabcolsep=1.8em
  \cyrfont
  \begin{tabular}{
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      @{\hskip 1em}}
    \toprule\midrule
    \multicolumn{2}{c}{\makebox[0pt]{\latfont Labial}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Alveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Postalveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Prepalatal}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Velar}} \\\midrule
    м & \ortho{m} & н     & \ortho{n}
    &&& њ   & \ortho{\'n} & ң & \ortho{ŋ} \\

    п & \ortho{p} & т     & \ortho{t} & тш & \ortho{\v{c}}
    &   ч   & \ortho{\'c} & к & \ortho{k} \\

      &           & с     & \ortho{s} & ш  & \ortho{\v{s}}
    &   щ   & \ortho{\'s} \\

    в & \ortho{w} & д л р & \ortho{d l r}
    &&& з й & \ortho{g j} & г & \ortho{v} \\\bottomrule
  \end{tabular}
\end{fullwidth}
\vspace{1em}

\noindent Me Kälemät's\marginnote{\cyrfont\normalsize\centering
  \begin{tabular}{llll}
    \toprule
    и ы & \ortho{i \"u}
    & \multicolumn{1}{r}{у} & \multicolumn{1}{r}{\ortho{u}} \\
    е   & \ortho{e}     & ъ о & \ortho{\"e o} \\
    э   & \ortho{\"a}   & а   & \ortho{a} \\\bottomrule
  \end{tabular}\\[0.5em]{\latfont Initial vowels}\\\vspace{1em}
  \begin{tabular}{llll}\toprule
    ы   & \ortho{e}     \\
    а э & \ortho{a \"a} \\\bottomrule
  \end{tabular}\\[0.5em]{\latfont Non-initial vowels}} Cyrillic orthography
contains only two letters not in the Russian alphabet, \ortho{\cyrfont њ} and
\ortho{\cyrfont ң}. It has a one-to-one mapping with the Latin orthography,
except for both \mk{\v{c}} and \mk{t\v{s}} corresponding to \ortho{\cyrfont тш}.
In practice, however, \mk{t\v{s}} never appears, even in loanwords. The example
concluding the previous section is written:
\bigexample{\cyrfont %
  «Елэ SMS·мегы·пелык ме auditorium:на.» монаймы Jonathan:ң.}{%
  ``Don't be afraid to send me an SMS at the auditorium,'' I told Jonathan.}

\subsection{Inuktitut syllabics}
\begin{fullwidth}
  \tabcolsep=1.8em
  \inufont
  \begin{tabular}{
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      l@{\hskip 1em}l
      @{\hskip 1em}}
    \toprule\midrule
      \multicolumn{2}{c}{\makebox[0pt]{\latfont Labial}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Alveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Postalveolar}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Prepalatal}}
    & \multicolumn{2}{c}{\makebox[0pt]{\latfont Velar}} \\\midrule
    ᒪ & \ortho{m} & ᓇ & \ortho{n}
    &&& ᓐᔭ & \ortho{\'n} & ᖓ & \ortho{ŋ} \\

    ᐸ & \ortho{p} & ᑕ & \ortho{t} & ᑦᖬ & \ortho{\v{c}}
    &   ᑦᔭ & \ortho{\'c} & ᑲ & \ortho{k} \\

      &           & ᓴ & \ortho{s} & ᖬ & \ortho{\v{s}}
    &   ᔅᔭ & \ortho{\'s} \\

    ᕙ & \ortho{w} & ᖤ ᓚ ᕋ & \ortho{d l r}
    &&& ᖦᔭ ᔭ & \ortho{g j} & ᒐ & \ortho{v} \\\bottomrule
  \end{tabular}
\end{fullwidth}
\vspace{1em}

\noindent Me Kälemät's Inuktitut syllabics orthography is its simplest and most
straightforward. Out of the consonants, \ortho{\inufont ᖓ} has a few special
cases: word-final \mk{-ŋ} is simply \ortho{\inufont ᖕ}, and the clusters \mk{ŋv}
and \mk{ŋk} are \ortho{\inufont ᙵ} and \ortho{\inufont ᖕᑲ} respectively.

Out of the eight stem-initial
vowels, \mbox{\mk{a i u e}} are represented as is usual for Inuktitut syllabics,
while \mbox{\mk{\"a \"e \"u o}} are associated with a modifying \ortho{\inufont
  ᕻ}. Stems beginning with vowels represent it as the full letter
\ortho{\inufont ᕹ}, while stems beginning with consonants use the full letter of
the consonant, followed by the modifying \ortho{\inufont ᕻ}, which precedes any
coda consonants.
\marginnote{\inufont\normalsize\centering
  \begin{tabular}{llll}
    \toprule
    ᐃ ᕷ & \ortho{i \"u}
    & \multicolumn{1}{r}{ᐅ} & \multicolumn{1}{r}{\ortho{u}} \\
    ᐁ   & \ortho{e}     & ᕵ ᕴ & \ortho{\"e o} \\
    ᕹ   & \ortho{\"a}   & ᐊ   & \ortho{a} \\\bottomrule
  \end{tabular}\\[0.5em]{\latfont Initial vowels}\\\vspace{1em}
  \begin{tabular}{llll}\toprule
    ᐃ & \ortho{e}     & ᐅ & \ortho{o} \\
    ᐊ & \ortho{a/\"a} \\\bottomrule
  \end{tabular}\\[0.5em]{\latfont Non-initial vowels}}

The orthography does not distinguish non-initial \mk{a} and \mk{\"a}, but is
otherwise similar to the Latin orthography in its choices. The example
concluding the previous sections is written:
\bigexample{\inufont %
  «ᐁᓚ SMS·ᒣᒋ·ᐯᓕᒃ ᒣ auditorium:ᓇ.» ᒣᕻᓇᔾᒥ Jonathan:ᖕ.}{%
  ``Don't be afraid to send me an SMS at the auditorium,'' I told Jonathan.}
\enlargethispage{2.5cm} \mk{Auditorium} remains unitalicized, both because it's
already obvious that it's a foreign word, and because Inuktitut syllabics fonts
rarely provide italics in the first place, Latin or not. Note though that
naturalized loanwords can be written with the syllabics themselves; for example,
the previously shown \mk{ipsevolovja} is rendered as {\inufont ᐃᑉᒍᓗᒡᔭ}.
\pagebreak
