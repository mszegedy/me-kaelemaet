\chapter{Verbs}
\section{Aspect}
\histnote{\mk{-k} is the widely-debated PU ``present tense suffix'' \recon{-k},
  reconstructed as an imperfective on account of its very bleached reflexes
  (such as forming the present tense in Mansi, being part of the simple
  causative in Hungarian and Forest Enets, and possibly forming a moderative in
  Eastern Khanty).

  \mk{-p} is the mysterious element of the PU \recon{-pta} causative,
  reconstructed as perfective moreso for reasons of symmetry than for
  significant motivation. It appears in Hungarian momentane \xeno{-t}
  (<~\recon{-pta}), and Mansi momentane \xeno{-p} (< \recon{-p}). \recon{-pta}
  is most well-known for forming simple causatives in a broad variety of Uralic
  languages, originally found in Samoyedic, and later found to be equivalent to
  \recon{-tta}~\citep{C,A}. It also forms a multiplicative in Khanty. Comparing
  these, we find ``perfective'' a reasonable stab at its original meaning.

  \mk{-ć} and \mk{-j} are the past tense suffixes listed in \citet{A}. The
  difference in telicity is mostly artistic license.}
\begin{affixestwodefs}{Aspects}{formal name}{description}
  -k   & imperfective & it happens over a period of time \\
  -p   & perfective   & it happens all at once \\
  -\'c & telic        & it happened and it got finished \\
  -j   & atelic       & it happened and it might not have gotten finished \\
\end{affixestwodefs}

\section{Mood}
\begin{affixes}{Moods}
  -k  & could or should happen \\
  -ne & hasn't happened \\
\end{affixes}

% -ne can have all the functions of finnic, mansi, and hungarian le-:
%   will be
%   will become
%   becomes
% in addition to just being a general irrealis/conditional mood

\section{Doer}
\subsection{Unknown or no victim}\label{s:indef-verb-endings}
\begin{affixes}{Doer endings for when we don't know the victim}
  -m        & me or us \\
  -t        & you \\
  -n        & them (with me)\\
  -\upa{∅} & them (not with me) \\\midrule
  -men      & me and people with me \\
  -ten      & you, with me \\
  -kan      & them, both with me and not with me \\\midrule
  -mat      & us \\
  -tat      & you guys \\
  -t        & them (pl.) \\
\end{affixes}

\subsection{Known victim}
\begin{affixes}{Doer endings for when we know the victim}
  -me  & me or us \\
  -te  & you \\
  -ne  & them (with me) \\
  -sa  & them (not with me) \\\midrule
  -men & me and people with me \\
  -ten & you, with me \\
  -san & them, both with me and not with me \\\midrule
  -mat & us \\
  -tat & you guys \\
  -sat & them (pl.) \\
\end{affixes}

\section{Non-finite forms}
\subsection{Gerunds and participles}\label{s:gerunds-and-participles}
\histnote{These gerunds are found in \citet{A}. It gives \recon{-nta} as a
  gerund, but does not give on opinion on the function of \recon{-śa} or
  \recon{-w} besides them being generic nominalizers. Our usages are mostly
  artistic license, but attempt to be consistent with the examples given.}
\begin{affixestwodefs}{Gerunds}{formal name}{description}
  -nta        & simple
  & the state of doing it, or getting it done to you \\
  -ća         & abilitative
  & the ability to do it, or get it done to you \\
  -w          & intentional
  & the intent to do it, or get it done to you \\
\end{affixestwodefs}

% nenets has -wi as its past participle, which is probably from inchoative *mə.
% just a reminder; i think "having gotten [V]" is a reasonable construction to
% add to mk

\begin{samepage}
\histnote{These participles are found in \citet{A}. It gives \recon{-pa} as a
  participle, but gives \recon{-ma} as a ``generic nominalizer''. We reconstruct
  it as an absolutive participle based on \morp{ptcp.abs-car.vbz-ptcp.abs} being
  the only reasonable interpretation for Ánte's \recon{-ma-kta-ma} that makes
  \recon{-kta-ma} (which forms caritive adjectives from nouns) and \recon{-ma}
  (which became a gerund or participle in many Uralic languages, including PSmy)
  make sense. As a bonus, it also explains why the \recon{-kta-k} ``abessive'',
  also given in \citet{A}, is case-like, making it actually a finite verb.}
\begin{affixestwodefs}{Participles}{formal name}{description}
  -ma         & absolutive
  & doing it, or getting it done to you \\
  -pa         & ergative
  & doing it to s'th \\
\end{affixestwodefs}
\end{samepage}

\subsection{Agent nouns}\label{s:agent-nouns}
\histnote{\mk{-ja} is straight from \citet{A}. For \mk{-kka}, \citet{Pkko}
  discusses the various etymologies of Finnish \textit{-kko}, some of them
  coming from \recon{-kka-j}. The three usages he cites for the latter are
  forming diminutive nominals (\textit{kolista} `clatter' > \textit{kolikko}
  `coin'), animate nominals (\textit{elää} `live' > \textit{elikko} `critter',
  \textit{emä} `mother animal' > \textit{emakko} `mother pig'), and action
  nominals (\textit{yltää} `reach' > \textit{ylläkkö} `assault (n.)'). Ánte
  cites a few examples, too, in which it is either bleached completely, or seems
  like an agent noun. Assuming that the \textit{yltää/ylläkkö} pair represents a
  conservative usage of the suffix, however, it seems like it indicates a causal
  relationship between the verb and the derived noun. For all the derived nouns,
  ``cause of \ortho{verb}'' makes sense as an interpretation, so that's what we
  went with. This also solves part of the puzzle of \citet{A}'s
  \recon{-kka-s(ə)}, though it's still unclear what the second element is. We
  have a moderative or semblative meaning for it, based on Ánte's inclinative
  interpretation of \recon{-kka-s(ə)}. See \S\ref{s:nominals-from-nominals}.}
\begin{affixes}{Endings to make agent nouns}
  -ja         & s'one who usually does it \\
  -kka        & cause of \\
\end{affixes}

\section{Evidentials}
\section{Auxiliary verbs}
\section{Derivation}
\subsection{From verbs}
\histnote{\mk{-le} is equivalent to \citet{A}'s \recon{-lə}. Given that it's
  inchoative (Tundra Nenets, Hungarian), imperfective (Khanty, Mansi), or
  bleached (Khanty) in East Uralic, and has a variety of imperfective meanings
  in West Uralic (atelic in Finnic, iterative in Sámic, moderative and perhaps
  continuative in Permic), we find ``inchoative'' a reasonable stab at its
  meaning~\citep{C}. The other suffixes are from \citet{A}, and retain Ánte's
  reconstructed meanings.}
\begin{affixestwodefs}{%
    Endings to make verbs from verbs}{formal name}{description}
  -we    & reflexive      & do it to or by yourself \\
  -je    & intentional    & do it, or get it done to you, intentionally \\
  -ta    & causative      & make s'one do it, or make it happen to s'one \\
  -le    & inchoative     & start doing it \\
  -nte   & durative       & keep doing the thing, or having it happen to you \\
  -k\'ce & multiplicative & do the thing a lot of times \\
\end{affixestwodefs}

\subsection{From nominals}\label{s:verbs-from-nominals}
\histnote{%
  We reconstruct \recon{-kta} as the common component of \citet{A}'s abessive
  \recon{-kta-k}, caritive adjective \recon{-kta-ma}, and negative participle
  \recon{-ma-kta-ma}. The semantics fit when \recon{-ma} is assumed to be an
  absolutive participle, and \recon{-k} is assumed to be a verb ending; see
  \S\ref{s:gerunds-and-participles}.

  The reflexes of PU \recon{-lə} vary widely, so it must have been a suffix that
  bleaches easily, or was already very general. For lack of a better option, we
  reconstruct ``use or make''~\citep{C}.}
\begin{affixes}{Endings to make verbs from nouns}
  -me  & become it \\
  -kta & lack it \\
  -je  & ? \\
  -ta  & ? \\
  -le  & use or make \\
\end{affixes}
