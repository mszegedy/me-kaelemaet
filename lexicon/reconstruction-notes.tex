\chapter{Reconstruction notes}
\section{The relationship of Me Kälemät to Proto-Uralic}
When discussing the historicity of Me Kälemät, it's important to distinguish the
three languages that are outlined in this document, to various degrees:
\begin{itemize}
  \item Proto-Uralic as reconstructed by experts, which is an entirely serious
    attempt made by people well-studied in historical linguistics and Uralic
    comparative linguistics;
  \item Proto-Uralic as reconstructed by us, which is entirely serious at times
    and does reflect a lot of careful thought, but overall has no guarantee of
    quality and is not meant to be our best effort at the task; and
  \item Me Kälemät, which is an object of fantasy and contains many details that
    were invented from whole cloth, filling in the gaps in our reconstruction of
    PU to make it a complete and aesthetically pleasing language, nevertheless
    appealing to our sense of realism.
\end{itemize}
There are a lot of one-to-one correspondences between the three, starting with
the phoneme inventory. Our reconstruction is not exotic in its inventory, and
has the same inventory as \citet{Ued}. The only exception in the one-to-one
correspondence is Me Kälemät having a separate \recon{ć} and \recon{ś}. We
choose to preserve \recon{ś} in Indo-Iranian loanwords, to make them more
distinct and superficially closer to the borrowed forms. Academically, however,
we favor Ánte and Pystynen's decision to unconditionally reconstruct \recon{ć}.
Hence, you will find only \recon{ć} in our reconstructions.

With that issue out of the way, we can list off the correspondences between
the inventories of our reconstruction and of Me Kälemät:

\vspace{1em}
\begin{fullwidth}%
  \tabcolsep=1.8em
  \ipa
  \begin{tabular}{c@{\hskip 4em}c}
    \multicolumn{2}{c}{%
      \begin{tabular}{
          @{\hskip 1em}
          l@{\hskip 1em}l
          l@{\hskip 1em}l
          l@{\hskip 1em}l
          l@{\hskip 1em}l
          l@{\hskip 1em}l
          @{\hskip 1em}}
        \toprule\midrule
        \multicolumn{2}{c}{\makebox[0pt]{Labial}}
        & \multicolumn{2}{c}{\makebox[0pt]{Alveolar}}
        & \multicolumn{2}{c}{\makebox[0pt]{Postalveolar}}
        & \multicolumn{2}{c}{\makebox[0pt]{Prepalatal}}
        & \multicolumn{2}{c}{\makebox[0pt]{Velar}} \\\midrule
        % nasals
        \colorrecon{m} & m &
        \colorrecon{n} & n &&&
        \colorrecon{ń} & ń &
        \colorrecon{ŋ} & ŋ \\
        % stops
        \colorrecon{p} & p &
        \colorrecon{t} & t &
        \colorrecon{č} & č &
        \colorrecon{ć} & ć &
        \colorrecon{k} & k \\
        % sibilants
        && \colorrecon{s} & s &
        \colorrecon{š} & š &
        (\colorrecon{ć} & ś) \\
        % sonorants
        \colorrecon{w} & w &
        \colorrecon{d} \colorrecon{l} \colorrecon{r} & d l r &&&
        \colorrecon{ď} \colorrecon{j} & g j &
        \colorrecon{x} & v
        \\\bottomrule
      \end{tabular}
    }%
    \vspace{0.5em}\\
    \multicolumn{2}{c}{Consonants}
    \\[1em]

    \begin{tabular}{llrr}
      \toprule
      \colorrecon{i} \colorrecon{ü} & i ü &
      \colorrecon{u} & u \\
      \colorrecon{e} & e &
      \colorrecon{ë} \colorrecon{o} & ë o \\
      \colorrecon{ä} & ä &
      \colorrecon{a} & a \\\bottomrule
    \end{tabular}
    &
    \begin{tabular}{cc}\toprule
      \colorrecon{ə} \colorrecon{o} & e o \\
      \colorrecon{ä} \colorrecon{a} & ä a \\\bottomrule
    \end{tabular}
    \vspace{0.5em}\\
    Initial vowels & Non-initial vowels
  \end{tabular}
\end{fullwidth}
\vspace{1em}

There's not much to say about this table. Like we said: it's one-to-one, except
for Me Kälemät \mk{ć} and \mk{ś} both corresponding to \recon{ć}. It should be
possible to deduce the corresponding reconstructed form from any Me Kälemät
root; in almost all cases, this has already been done for you. Some Me Kälemät
roots, such as \mk{noke} `Arctic fox', have been reverse-engineered from single
branches, and cannot be said to have corresponding PU forms at all.
Nevertheless, in these cases, the hypothetical form is still provided, just with
a \colorhypoth{} rather than \colorrecon{}. (In the case of \mk{noke}, the form
is \hypoth{nokə}, from Janhunen's PNSmy~\recon{nokå}.)

\noindent In terms of vocabulary and grammar, there are a few surprises buried
in our grammar and lexicon, such as our analysis of privative \recon{kta} as a
verb-forming suffix; of \recon{ma} and \recon{pa} as forming an active-antipassive
participle pair; and some idiosyncratic reconstructions, such as \recon{käjə}
for ``court, sing'', \recon{oxjə} for ``swim'', and \recon{ńulxa} for ``silver
fir''. But we stick to well-reasoned and up-to-date reconstructions wherever
possible.

Our efforts are similar in scope to Ánte's goals in writing the \textit{Uralic
  Etymological Dictionary}, though he's a much more trustworthy source than we
are, and the only thing stopping us from copying the UED's roots wholesale is
that it hasn't actually come out yet. There are the above-mentioned cases where
we fancifully try to play Uralicist and improve established reconstructions with
our fresh pair of eyes, but we don't have any illusions of actually producing
anything academically valuable besides a modern dictionary of Proto-Uralic,
which will soon be superceded by the UED anyway.

If we were to make an actual effort to defend our reconstructions, we'd first
have to put together a case for permitting such strange clusters in PU as
\recon{xj} and \recon{lx}, and to make sure that the reflexes of the roots we're
reconstructing aren't better explained by downstream loans. But we're not
writing a graduate thesis here; we're just doing a project in our spare time.
Thus we'll never consult anything by Itkonen, for example; his works aren't
available online, and we're not going to buy our own copies or figure out how to
obtain them through our university library, especially not now that they're
closed until the pandemic blows over. We're also not putting together any
brand-new roots that haven't been connected yet; we lack the wherewithal and the
resources to pore over all the necessary dictionaries and wordlists for these
things. We might be more enthusiastic about doing so if we had Itkonen's
\textit{Suomen sanojen alkuperä} on hand\textellipsis~but we don't.

Perhaps the most thoroughly academically-sourced part of this documentation is
our reflex tables in App.~\ref{ch:reflex-tables} for, as much as we can manage,
every extant sound pattern in our reconstruction. While it does contain our
questionable consonant clusters as sources, shaky and speculative sources are
always clearly marked, and we try to provide good sources for every single cell
in our tables. This isn't just us trying to be good scholars; we're getting
really damn tired of simultaneously paging through the UED, Janhunen's
\textit{Samojedischer Wortschatz}, Sammallahti's ``Historical phonology of
the Uralic languages'', the other works in \textit{The Uralic Languages},
simultaneously Uralonet, Uralothek, and the paper UEW\textellipsis~there's an
endless amount of works to consult when we're trying to obtain trustworthy
reconstructions, so it's beneficial to our sanity to have everything in one
place. The products of these efforts may or may not be useful to you, depending
on who you are. But that's not really our goal.

\section{Our impression of Uralic diachronics}
We used to be pretty agnostic as to intermediate-level groupings in Uralic, but
we've ended up finding a lot of groupings that seem, if not plausible, at least
useful, in the sense that they're united by shared sound changes and unique
vocabulary. This in turn means that it's helpful to give these groups
hypothetical proto-languages, and keep track of their roots and sound changes.
This is more than a theoretical exercise; \citet{Ay} was able to convincingly
explain many Yukaghir roots as \emph{pre}-Samoyedic loans, based on the
hypothesis that pre-Samoyedic had \recon{ϑ} (> PSmy~\recon{t}) as its reflex of
PU~\recon{s}, as has been a long-standing assumption about Proto-Ugric or
Proto-Ob-Ugric. Even if these aren't real genetic groupings, keeping tabs on
areal developments this way is very important for being able to reason about
historical contacts like the ones between pre-Samoyedic and Yukaghir.

\begin{minipage}{\textwidth}
\noindent These are the groupings we find worthwhile to keep track of in such a
way:

\vspace{0.5em}
\begin{forest}
  for tree={grow=0, anchor=west},
  forked edges,
  [Uralic
    [East Uralic
      [Samoyedic, tier=pre-leaf,
        [South Samoyedic, tier=leaf]
        [North Samoyedic, tier=leaf]
      ]
      [Ob-Ugric
        [Khanty, tier=leaf]
        [Mansi, tier=leaf]
      ]
      [Hungarian, tier=leaf]
    ]
    [Permic, tier=pre-leaf,
      [Komi, tier=leaf]
      [Udmurt, tier=leaf]
    ]
    [Mari, tier=leaf]
    [West Uralic
      [Mordvinic, tier=pre-leaf,
        [Moksha, tier=leaf]
        [Erzya, tier=leaf]
      ]
      [Finno-Sámic, tier=pre-leaf,
        [Finnic, tier=leaf]
        [Sámic, tier=leaf]
      ]
    ]
  ]
\end{forest}
\vspace{0.5em}
\end{minipage}

\noindent These are the shared sound changes that characterize the groupings
that aren't universally accepted:
\begin{description}
  \item[West Uralic:] Merged \recon{ë-a} and \recon{a-a}.
    \begin{description}
      \item[Finno-Sámic:] Developed consonant gradation.
    \end{description}
  \item[East Uralic:] Had \recon{ć} > \recon{s}, with PU~\recon{s} becoming
    something weird instead (\upa{∅} in Hungarian, \recon{t} in Mansi and
    Samoyedic, and \recon{ʟ} in Khanty).
    \begin{description}
      \item[Ob-Ugric:] Turned \recon{u} into reduced vowels, and \recon{a-a}
        into \recon{ū}; also, merged PU~\recon{s} and PU~\recon{š}.%
        \marginnote{The merger of \recon{s} and \recon{š} is a trait shared with
          Hungarian, but there are very few roots containing \recon{š} with
          reflexes in Hungarian to support this. The only one we know of is
          \recon{šiŋxərə}, reflected as \xeno{egér}, which indeed reflects it as
          \upa{∅}, same as \recon{s}.}
    \end{description}
\end{description}
\noindent These groupings all have a lot of vocabulary exclusive to them as
well, and there's a lot of loan strata exclusive to each group (Finno-Sámic is
well-known for this, but East Uralic and Ob-Ugric are equally good contenders).
We're not going to explicitly reconstruct proto-languages for these groupings,
but it's sometimes worth thinking in these terms, and you may find us
referencing things like ``EU~\hypoth{ϑ}'' (in which EU stands for East Uralic).
