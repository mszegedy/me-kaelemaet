#!/usr/bin/env python3

import sys
import re
import time
import textwrap as tw
import itertools
import copy
from functools import (reduce, total_ordering)
from enum import Enum
from collections import OrderedDict

WORD_CLASSES = {
    'N' :'n.',
    'Na':'adj.',
    'Ns':'space word',
    'Nu':'numeral',
    'K' :'kinship term',
    'P' :'particle',
    'V' :'v.',
    'Va':'aux.~v.',
    'Vi':'imp.~v.',
}
WORD_CLASSES_RE = '(' + '|'.join(WORD_CLASSES.keys()) + ')'

# ( ((S\??)?(F\??)?(Md\??)?(M\??)?(P\??)?'
# (H\??)?(Ms\??)?(Kh\??)?(Smy\??)?))?'

FAMILIES = OrderedDict((
    ('S'  ,'Sá'),
    ('F'  ,'Fi'),
    ('Md' ,'Md'),
    ('M'  ,'Mr'),
    ('P'  ,'P'),
    ('H'  ,'Hu'),
    ('Ms' ,'Ms'),
    ('Kh' ,'Kh'),
    ('Smy','Smy'),
    ('Y'  ,'Yuk'),
    ('Nen','Nen'),
    ('Km' ,'Kam'),
))
FAMILIES_RE = \
    '((' \
    + '|'.join(key + r'\??' for key in FAMILIES.keys()) \
    + ')+)'

ASCII_EQUIVALENTS = {
    'ä':'ae',
    'ć':'cy',
    'č':'cz',
    'ë':'eo',
    'ń':'ny',
    'ŋ':'nz',
    'ś':'sy',
    'š':'sz',
    'ü':'ue',
}

def ascii_tuple_from_str(s):
    return tuple(ASCII_EQUIVALENTS.get(c,c) for c in s)

def ascii(s):
    return ''.join(ascii_tuple_from_str(s))

def split_on_capitals(s):
    return re.findall('[A-Z][^A-Z]*', s)

def sources_to_set(sources):
    return frozenset(split_on_capitals(sources))

class WordClasses:
    def __init__(self, class_symbols):
        if isinstance(class_symbols, str):
            self._set = frozenset(split_on_capitals(class_symbols))
        if isinstance(class_symbols, (set, frozenset)):
            self._set = frozenset(class_symbols)
    def __hash__(self):
        return hash(self._set)
    def __iter__(self):
        return iter(self._set)
    def __len__(self):
        return len(self._set)
    def __and__(self, other):
        return WordClasses(self._set & frozenset(other))
    def __rand__(self, other):
        return WordClasses(self._set & frozenset(other))
    def __or__(self, other):
        return WordClasses(self._set | frozenset(other))
    def __ror__(self, other):
        return WordClasses(self._set | frozenset(other))
    def __sub__(self,other):
        return WordClasses(self._set - frozenset(other))
    def __str__(self):
        return ''.join(sorted(self._set))
    @property
    def fset(self):
        return frozenset(self)
    @property
    def pretty_str(self):
        return ', '.join(WORD_CLASSES[s] for s in sorted(self._set))

@total_ordering
class Root:
    def __init__(self, name, word_classes, etymology_type, sources, reflexes,
                 header):
        self.name = name
        self.word_classes = word_classes
        self.etymology_type = etymology_type
        self.sources = sources_to_set(sources)
        self.reflexes = reflexes
        self.protoforms = []
        self.text = header + '\n'
        self.definitions = {} # WordClasses : 'definition'
        self.irldefs     = {} # WordClasses : 'definition'
        self.refs        = []
        self.note        = None
        self.index       = float('inf') # larger than any integer
        self.is_altform  = False
    @staticmethod
    def term_body(terms, word_classes):
        '''Creates a body for the terms that have any of the given word
        classes.'''
        term_tuple =  tuple(v for k,v in terms.items() \
                            if word_classes & k)
        if not len(term_tuple):
            return None
        return ', '.join(
            f'{i+1}.~{d}' for i,d in enumerate(term_tuple)
        ) if len(term_tuple) > 1 else term_tuple[0]
    def _compare(self):
        return (len(self.definitions.keys()) != 0,
                ''.join(s[0] for s in sorted(self.word_classes)),
                ascii_tuple_from_str(self.name))
    def _is_valid_operand(self, other):
        return isinstance(other, Root)
    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self._compare() == other._compare()
    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self._compare() < other._compare()
    def clear_for_altform(self, altform_name, altform_classes):
        word_class_groups = set(g & self.word_classes \
                                for g in WORD_CLASS_GROUPS \
                                if g & self.word_classes)
        self.refs = [f'<{self.name}|{str(c)}>' for c in word_class_groups]
        self.name    = altform_name
        if altform_classes is not None:
            self.word_classes = altform_classes
        self.defs    = {}
        self.irldefs = {}
        self.note    = None
        self.is_altform = True
    def finalize(self):
        self.word_classes = self.word_classes \
            | reduce(lambda x,y: x|y,
                     self.definitions.keys(),
                     WordClasses(set())) \
            | reduce(lambda x,y: x|y,
                     self.irldefs.keys(),
                     WordClasses(set()))
    def line_from_refs(self):
        return f'See also {", ".join(self.refs)}. \\par\n' if self.refs else ''
    def entries(self, word_classes):
        '''Returns a definition entry for a root if its word class intersects
        with the query word classes.'''
        our_word_classes   = self.word_classes & word_classes
        if not len(our_word_classes):
            return None
        other_word_class_groups = set(g & self.word_classes \
                                      for g in WORD_CLASS_GROUPS \
                                      if g != word_classes \
                                      and g & self.word_classes)
        def_body     = Root.term_body(self.definitions, word_classes)
        if def_body is None:
            return None
        irldef_body  = Root.term_body(self.irldefs, word_classes)
        self.refs.extend(f'<{self.name}|{str(c)}>' \
                         for c in other_word_class_groups)
        ref_line = self.line_from_refs()
        return Entry(self.name,           # name
                     our_word_classes,    # word_classes
                     self.etymology_type, # etymology_type
                     self.sources,        # sources
                     self.reflexes,       # reflexes
                     self.protoforms,     # protoforms
                     self.index,          # index
                     def_body,            # definition
                     irldef_body,         # irldef, V-- note
                     ref_line + (self.note if self.note else ''))


@total_ordering
class Entry:
    FAMILY_SPLITTER = re.compile(r'([A-Za-zá]+)(\?)?', re.UNICODE)
    UEW_MATCHER     = re.compile(r'UEW ([0-9]+)/([0-9]+)', re.UNICODE)
    CAPS_THE_UEW_MATCHER = re.compile(r'([.?!] +|\n)@R', re.UNICODE)
    LINK_MATCHER = re.compile(
        r'([^\\])<([^<>|\n\\]+)(|([^<>\n ]*))?>',
        re.UNICODE)
    CITE_MATCHER = re.compile(
        r'@(\()?'
        r'([A-Z][A-Za-z0-9]*)'
        r'\b(\))?( ([0-9]+))?',
        re.UNICODE)
    RECON_MATCHER   = re.compile(
        r'([*#])([^,;.?!{}*\\~ \t\n]+|\{([^}]+)\})',
        re.UNICODE)
    ESCAPE_MATCHER  = re.compile(r'\\(<)') # this is the only esc char so far
    def __init__(self, name, word_classes, etymology_type, sources, reflexes,
                 protoforms, index, definition, irldef=None, note=None):
        self.name           = name
        self.word_classes   = word_classes
        self.etymology_type = etymology_type
        self.sources        = sources
        self.reflexes       = reflexes
        self.protoforms     = protoforms
        self.index          = index
        self.definition     = definition
        self.irldef         = irldef
        self.note           = note

    @staticmethod
    def hyperform(name, word_classes):
        return ascii(name) + str(word_classes)

    @staticmethod
    def link_replacement(matches, default_classes):
        prefix = matches.group(1)
        match = matches.group(2)
        if matches.group(3):
            word_classes = WordClasses(matches.group(4))
            return prefix + r'\hyperlink{' \
                   + Entry.hyperform(match, word_classes) \
                   + r'}{\entryword{' + match + r'}~\entryclasses{(' \
                   + word_classes.pretty_str + r')}}'
        return prefix + r'\hyperlink{' \
               + Entry.hyperform(match, default_classes) \
               + r'}{\entryword{' + match + '}}'
    @staticmethod
    def substitute_links(text, default_classes=None):
        return Entry.LINK_MATCHER.sub(
            lambda x: Entry.link_replacement(x, default_classes),
            text)

    @staticmethod
    def cite_replacement(matches):
        match = matches.group(0)
        if match == '@R':
            return r'the \uew{}'
        if match == '@(R)':
            return r'\uewp{}'
        match   = matches.group(2)
        names   = ','.join(split_on_capitals(match))
        command = r'\citet'
        coda    = ''
        if matches.group(1):
            command = r'\citep'
            if not matches.group(3):
                print('Warning: mismatched parens in citation',
                      matches.group(0))
        elif matches.group(3):
            coda = ')'
        page = matches.group(5)
        if page is not None:
            return f'{command}[p.\\,{page}]{{{names}}}{coda}'
        return f'{command}{{{names}}}{coda}'
    @staticmethod
    def substitute_citations(text, page=None):
        return Entry.CITE_MATCHER.sub(Entry.cite_replacement, text)

    @staticmethod
    def reconstruction_replacement(matches, color):
        command = {'*': r'recon', '#': r'hypoth'}[matches.group(1)]
        content = matches.group(3) or matches.group(2)
        if color:
            return r'\color' + command + r'{' + content + r'}'
        return '\\' + command + r'{' + content + r'}'
    @staticmethod
    def substitute_reconstructions(text, color=False):
        return Entry.RECON_MATCHER.sub(
            lambda x: Entry.reconstruction_replacement(x, color),
            text)

    @staticmethod
    def filtered_reflexes(reflexes):
        if not reflexes:
            return ''
        prefix = r'\entryreflexes{'
        if reflexes[0] == '>':
            reflexes = reflexes[1:]
            prefix = '>~' + prefix
        suffix = '}'
        return prefix \
            + ' '.join(
                FAMILIES[family] + qmark \
                for family, qmark in (
                    Entry.FAMILY_SPLITTER.findall(key)[0] \
                    for key in split_on_capitals(reflexes))) \
            + suffix

    def __str__(self):
        # intentionally deferring default behavior to to_str():
        return self.to_str()
    def to_str(self, suppress_class=False):
        # for hyperlinks, but also for minimum-effort error reporting
        hyperform = Entry.hyperform(self.name, self.word_classes)
        word_classes_text = '' if suppress_class \
                            else f'({self.word_classes.pretty_str})'
        source_text = {'@': 'from~',
                       '*': 'our reconstruction',
                       '°': 'reverse-engineered',
                       '¢': 'our coinage',
                       '«': 'our loan'}[self.etymology_type]
        if self.etymology_type == '@':
            source_text += r', '.join(r'\entrycite{' + s + r'}' \
                                      for s in sorted(self.sources))
        protoforms_text = ''
        if self.protoforms:
            for form, reflexes, source in self.protoforms:
                reflexes = ' '+ Entry.filtered_reflexes(reflexes) \
                           if reflexes is not None \
                           else ''
                form = Entry.substitute_reconstructions(form, color=True)
                # i know this looks overly clever but it helps me stay sane
                source = {
                    '"': r'\ditto',
                    '*': r'(our reconstruction)',
                    '°': r'(our reverse-engineering)',
                }.get(
                    source,
                    Entry.UEW_MATCHER.sub(
                        r'UEW \1 (\\#\2)',
                        Entry.substitute_citations(source)))
                protoforms_text += \
                    r'    \entryprotoform{' + form + reflexes + r'}{' \
                    + source + '}%\n'
            protoforms_text = '  \\entryprotoforms{%\n' + protoforms_text + '}'
        # the fstring version of this was, just, unimaginably cursed. so, it's
        # rstrings concatenated together with the insertions. you're welcome.
        header = r'\entry{\hypertarget{' + hyperform + r'}{'\
                 + self.name + r'}}{' + word_classes_text + r'}{' \
                 + source_text + r'}{' \
                 + Entry.filtered_reflexes(self.reflexes) + '}{%\n' \
                 + protoforms_text + '}{%\n'
        definition_text = tw.indent(
            tw.fill(self.definition,
                    break_long_words=False,
                    break_on_hyphens=False,
                    width=75), # 79 after you add the indent
            '    ')
        note_text = None
        if self.note:
            if self.note.startswith('@R'):
                self.note = r'The \uew{}' + self.note[2:]
            self.note = Entry.CAPS_THE_UEW_MATCHER.sub(
                r'\1The \\uew{}',
                self.note
            )
            note_text = Entry.substitute_links(self.note, self.word_classes)
            note_text = Entry.substitute_citations(note_text)
            note_text = Entry.substitute_reconstructions(note_text)
            note_text = Entry.ESCAPE_MATCHER.sub(r'\1', note_text)
            note_text = tw.indent(
                tw.fill(note_text,
                        break_long_words=False,
                        break_on_hyphens=False,
                        width=75), # 79 after you add the indent
                '    ')
        definition = f'  \\entrydef{{%\n{definition_text}}}\n'
        irldef = f'  \\entryirldef{{{self.irldef}}}\n' if self.irldef else ''
        note = f'  \\entrynote{{%\n{note_text}}}\n' if note_text else ''
        footer = '}\n'
        return header + definition + irldef + note + footer
    def _compare(self):
        return (self.index, ascii_tuple_from_str(self.name))
    def _is_valid_operand(self, other):
        return isinstance(other, Entry)
    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self._compare() == other._compare()
    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self._compare() < other._compare()

ENTRY_LINE_MATCHER = re.compile(f' *%%%%({WORD_CLASSES_RE}+)%%%% *')
template_lines = []
word_class_groups = set()
for line in open('roots.tex.template'):
    matches = ENTRY_LINE_MATCHER.fullmatch(line[:-1])
    if not matches:
        template_lines.append(line)
        continue
    word_classes = WordClasses(matches.group(1))
    template_lines.append(word_classes) # this is lazy but it works
    word_class_groups.add(word_classes)

TEMPLATE_LINES = tuple(template_lines)
del template_lines
WORD_CLASS_GROUPS = frozenset(word_class_groups)
del word_class_groups
# we're just neurotic about this stuff. it doesn't actually improve anything.

roots = []

# 'INC' here stands for "incomplete"
ParseMode = Enum('ParseMode',
                 'NEW_ENTRY PROTOFORMS DEFS INC_DEF INC_IRLDEF INC_NOTE')
HEADER_MATCHER = re.compile(
    r'([^ ]+) (' + WORD_CLASSES_RE + r'+) ([@*°¢«])([A-Za-z0-9]*)' \
    r'( ' + FAMILIES_RE + r')?' \
    r'( UEW [0-9, ]+)?',
    re.UNICODE)
PROTOFORM_MATCHER = re.compile(
    r'([A-Za-zá\-]+ )?(\?)?([*#])(.+?)' \
    r'( >' + FAMILIES_RE + r')?' \
    r' +(@?[A-ZÁ][A-ZÁa-zá0-9]* +[0-9/]+|@[A-ZÁ][A-Za-zá0-9]*'
    r'|Álgu sátnebearaš +[0-9]+|["*°])')
INDEX_MATCHER     = re.compile(r'#(-?[0-9]+)', re.UNICODE)
# SPELLING_MATCHER  = re.compile(r'& *([A-Za-z0-9\-_]+)', re.UNICODE)
DEF_MATCHER = re.compile(
    r'(!)?((' \
    + WORD_CLASSES_RE \
    + r'+) +)?([^*+<].*)', re.UNICODE)
ALTFORM_MATCHER = re.compile(
    r'\+altform ([^ ]+)( (' \
    + WORD_CLASSES_RE + r'+))?', re.UNICODE)
CROSSREF_MATCHER  = re.compile(r'<([^<>|]+)(|([^<>]*))?>', re.UNICODE)
NOTE_MATCHER      = re.compile(r'\*(.*)', re.UNICODE)

parse_mode    = ParseMode.NEW_ENTRY
altform_count = 0
key           = None # word class of current definition
text          = ''

def clear():
    global key
    global text
    key  = None
    text = ''

def finalize_definition(is_irldef):
    global roots
    global parse_mode
    global key
    global text
    if is_irldef:
        roots[-1].irldefs[key] = text
    else:
        roots[-1].definitions[key] = text
    clear()

# maybe one day we'll reimplement this with an actual ebnf grammar. but, not
# today. for now just check the regexes; it's not hard to figure out.
for line in open('roots.txt'):
    line = line.rstrip('\n')

    if parse_mode == ParseMode.NEW_ENTRY:
        matches = HEADER_MATCHER.fullmatch(line)
        if matches:
            reflexes = matches.group(7)
            reflexes = '' if reflexes is None else reflexes
            roots.append(
                Root(
                    matches.group(1),              # name
                    WordClasses(matches.group(2)), # word classes
                    matches.group(4),              # etymology type
                    matches.group(5),              # sources
                    reflexes,                      # reflexes
                    line))
            parse_mode = ParseMode.PROTOFORMS
            continue
        continue
    try:
        roots[-1-altform_count].text += line + '\n'
    except IndexError:
        pass

    if line == '':
        roots[-1].finalize()
        parse_mode    = ParseMode.NEW_ENTRY
        altform_count = 0
        clear()
        continue

    # the only other kind of state possible here is an incomplete text
    if parse_mode in (ParseMode.PROTOFORMS, ParseMode.DEFS):

        matches = ALTFORM_MATCHER.fullmatch(line)
        if matches:
            roots.append(copy.deepcopy(roots[-1-altform_count]))
            if altform_count == 0:
                roots[-2].finalize() # overwrites word classes
                altform_count = 1
            else:
                altform_count += 1
            name = matches.group(1)
            word_class = matches.group(3)
            if word_class is not None:
                word_class = WordClasses(word_class)
            roots[-1].clear_for_altform(name, word_class)
            if word_class is not None:
                roots[-1-altform_count].refs.append(
                    f'<{name}|{str(word_class)}>')
            else:
                roots[-1-altform_count].refs.append(f'<{name}>')
            clear()
            parse_mode = ParseMode.PROTOFORMS
            continue

        # PROTOFORMS state means we haven't seen a definition yet
        if parse_mode == ParseMode.PROTOFORMS:
            matches = PROTOFORM_MATCHER.fullmatch(line)
            if matches:
                form = (matches.group(1) or '') \
                       + (matches.group(2) or '') \
                       + matches.group(3) \
                       + matches.group(4)
                roots[-1].protoforms.append((
                    form,
                    (matches.group(5) or '').strip(),
                    matches.group(8)))
                continue
            matches = INDEX_MATCHER.fullmatch(line)
            if matches:
                roots[-1].index = int(matches.group(1))
                continue

        # matches any line that doesn't begin with one of [*+<]
        matches = DEF_MATCHER.fullmatch(line)
        if matches:
            key = WordClasses(matches.group(3)) if matches.group(3) \
                  else roots[-1].word_classes
            text = matches.group(5)
            if text[-1] == '\\':
                text = text[:-1]
                parse_mode = ParseMode.INC_IRLDEF if matches.group(1) \
                             else ParseMode.INC_DEF
                continue
            finalize_definition(matches.group(1))
            parse_mode = ParseMode.DEFS
            continue

        if parse_mode == ParseMode.DEFS:
            matches = CROSSREF_MATCHER.fullmatch(line)
            if matches:
                roots[-1].refs.append(line)
                continue
            matches = NOTE_MATCHER.fullmatch(line)
            if matches:
                # text buffer should be '' but this leaves the door open for
                # adding prefixes to the note in the above code
                text += matches.group(1)
                if text[-1] == '\\':
                    text = text[:-1]
                    parse_mode = ParseMode.INC_NOTE
                    continue
                roots[-1].note = text
                continue
        continue

    if parse_mode in (ParseMode.INC_DEF, ParseMode.INC_IRLDEF,
                      ParseMode.INC_NOTE):
        text += line.lstrip()
        if text[-1] == '\\':
            text = text[:-1]
            continue
        if parse_mode in (ParseMode.INC_DEF, ParseMode.INC_IRLDEF):
            finalize_definition(parse_mode == ParseMode.INC_IRLDEF)
            parse_mode = ParseMode.DEFS
            continue
        roots[-1].note = text
        parse_mode = ParseMode.PROTOFORMS

roots.sort()
print(f'Read {len(roots)} roots:')
print('   nominals:', len([root for root in roots \
                           if root.word_classes & {'N', 'Na', 'Ns', 'Nu'}]))
print('    verbals:', len([root for root in roots \
                           if root.word_classes & {'V', 'Vi', 'Va'}]))
print('   kinships:', len([root for root in roots \
                           if root.word_classes & {'K'}]))
print('  particles:', len([root for root in roots \
                           if root.word_classes & {'P'}]))
with open('roots.txt', 'w') as roots_file:
    for root in roots:
        if not root.is_altform:
            roots_file.write(root.text)
            if root.text[-2:] != '\n\n':
                roots_file.write('\n')

entries = {}
with open('roots.tex', 'w') as tex_file:
    for line in TEMPLATE_LINES:
        if isinstance(line, str):
            tex_file.write(line)
            continue
        # otherwise, the "line" is actually a WordClasses object
        these_entries = []
        for root in roots:
            entry = root.entries(line)
            if entry is not None:
                these_entries.append(entry)
        these_entries.sort()
        suppress_class = len(line) <= 1
        for entry in these_entries:
            tex_file.write(entry.to_str(suppress_class))
            tex_file.write('\\entryspacer\n')
        entries[line] = these_entries
# check for duplicates (and, in future versions, broken links)
n_entries = 0
for line, these_entries in entries.items():
    n_entries += len(these_entries)
    names = tuple(e.name for e in these_entries)
    for name in frozenset(names):
        count = names.count(name)
        if count > 1:
            print(f'Entry <{name}> has {count-1} duplicate(s).')
print(f'Wrote {n_entries} entries.')
